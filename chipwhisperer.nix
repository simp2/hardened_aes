{ lib
, fetchPypi
, buildPythonPackage
, pythonOlder
, tqdm
, libusb1
, pyserial
, cython
, ecpy
, pyusb
, numpy
, scipy
, fastdtw
, configobj
}:

buildPythonPackage rec {
  pname = "chipwhisperer";
  # version = "5.5.2";
  version = "5.7.0";
  # version = "5.6.1";

  disabled = pythonOlder "3.8";

  src = fetchPypi {
    inherit pname version;
    # sha256 = "sha256-Gi12wxSOEkqjVWklYIoNEQGox6C5aFSQMikG0rf/ROs="; # 5.6.1
    # sha256 = "sha256-amUzHZ2EWCrKRm4FowPUrxYhcI3ifLn3bxAo/T7SXoQ="; #5.5.2
    sha256 = "sha256-xTRUkdo09xKIEGITmEEHSVBd7cqrtD72lR/kklSt7Z8="; # 5.7.0
  };

  propagatedBuildInputs = [
    tqdm
    pyserial
    cython
    ecpy
    pyusb
    numpy
    scipy
    fastdtw
    configobj
    libusb1
  ];

  doCheck = false;

  pythonImportsCheck = [ "chipwhisperer" ];

  meta = with lib; {
    description = "ChipWhisperer Side-Channel Analysis Tool";
    homepage = "https://github.com/newaetech/chipwhisperer";
    license = licenses.gpl3;
  };
}
