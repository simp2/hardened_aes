# cw308_aes

## Installation

- Nécessite ``nix`` installé.
- Ajouter les udev rules nécessaires pour le support des ChipWhisperer : ``sudo ./install/install_cw_usb.sh``.

## Présentation du TP

Le but de ce TP est l'implémentation d'un chiffrement AES durci, c'est-à-dire protéger contre les attaques par fautes et par observation. Les attaques seront réalisées en pratique.

Pour cela :

- Implémenter un AES dans le répertoire *myaes*. Il y a une structure présente qui **peut** être modifiée du moment que le trigger est conservé pour la synchronisation des attaques.
- Il y a des scripts pythons présents (et des scripts wrappers) pour mesurer des traces et injecter des fautes dans les dossiers respectifs *sca* et *fia*.
- Échanger votre implémentation d'AES, y compris les sources, avec une autre personne : vous devenez évaluateur de cette implémentation. Essayez de retrouver la clé avec les techniques que vous avez apprises.
- N'hésitez pas à échanger les attaques entre vous.
- En cas de vulnérabilité trouvée, mettre à jour l'implémentation AES.

## Compte-rendu

Le compte rendu se fait en deux parties :
- Une description de votre implémentation et des contremesures utilisées.
- Un audit de l'implémentation AES reçue.


## Branchement

1. USB PC vers CW-Capture
2. Nappe 20 broches entre CW-Capture et CW308
3. Cable coaxial entre CW-Capture "Measure" et CW-308

## Utilisation

Compilation du binaire :
```
./build.sh
```

Programmation du microcontrôleur :
```
./burn.sh
```

## Test du bon fonctionnement du chiffrement

Lancer le script de test, avec la carte branchée :

```
./test.sh
```

## Exemple d'attaque SCA

Mesure des traces :
```
./sca/traces2000.sh
```


