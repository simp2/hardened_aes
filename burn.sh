#!/usr/bin/env nix-shell
#!nix-shell default.nix -i python3

import chipwhisperer as cw
scope = cw.scope()
scope.default_setup()
prog = cw.programmers.STM32FProgrammer
cw.program_target(scope, prog, "myaes/aes.hex")
