with import <nixpkgs> {};

let
     pkgs = import (builtins.fetchGit {           
        name = "pinned_nix_packages";                                                 
        url = "https://github.com/nixos/nixpkgs/";                       
        ref = "nixos-23.11";                     
        rev = "205fd4226592cc83fd4c0885a3e4c9c400efabb5";                                           
    }) {};                                                                        
in

let
  chipwhisperer_pkg = ps: ps.callPackage ./chipwhisperer.nix {};
  pythonEnv = pkgs.python310.withPackages (ps: [
    ps.matplotlib
    ps.tqdm
    ps.numpy
    ps.pycrypto
    (chipwhisperer_pkg ps)
  ]);
in

 # Make a new "derivation" that represents our shell
pkgs.pkgsCross.arm-embedded.mkShell {
  name = "dev-env";

  nativeBuildInputs = with pkgs; [
    pythonEnv
    bash
    libusb1
    julia-bin
    # jupyter
  ];

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
   
  ];
}