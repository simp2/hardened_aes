'''
File: trace1.py
Created Date: Friday December 17th 2021
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Friday, 30th September 2022 2:52:17 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2021
'''

import chipwhisperer as cw
import matplotlib.pyplot as plt
import time

def reboot_flush(scope, target):
        scope.io.nrst = False
        time.sleep(0.1)
        target.flush()
        scope.io.nrst = "high_z"
        time.sleep(0.1)
        # get 1 byte because of improper UART setup procedure
        target.flush()
        c = target.read(num_char=1, timeout=250).encode('utf-8')
        


# connect to chipwhisperer
scope = cw.scope()
# setup scope with default parameters
# Sets up sane capture defaults for this scope
#         25dB gain
#         5000 capture samples
#         0 sample offset
#         rising edge trigger
#         7.37MHz clock output on hs2
#         4*7.37MHz ADC clock
#         tio1 = serial rx
#         tio2 = serial tx
#         CDC settings change off


scope.default_setup()
scope.adc.samples = 1000


# target = microcontroller
target = cw.target(scope)

reboot_flush(scope, target)
ktp = cw.ktp.Basic()
key, pt = ktp.new_pair()

# target.write(b"U")
target.simpleserial_write('t', bytearray([1])) # set targeted round as 1
target.simpleserial_wait_ack()

target.set_key(key)

trace = cw.capture_trace(scope, target, pt, key)
plt.plot(trace.wave)
plt.show()

